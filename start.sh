#!/bin/bash
clear
if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi
export TOKEN
export PREFIX
export CLIENT_ID
export CLIENT_SECRET

pkill -f PDBot-py3 
pkill -f PDBot-rb
pkill -f PDBot-c
git fetch; git pull
bash -c "exec -a PDBot-py3 python3 dbot.py" & bash -c "exec -a PDBot-rb ruby rbot.rb" & bash -c "exec -a PDBot-c ./node_modules/coffeescript/bin/coffee cbot.coffee"
bash stop.sh
