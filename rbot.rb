require 'discordrb'
require 'dotenv'
require 'json'
require 'yaml'
require 'fileutils'

FileUtils.mkdir_p "config"
FileUtils.touch(File.join("config", "serverticketchans.yaml"))
FileUtils.touch(File.join("config", "latestticket.cfg"))
FileUtils.touch(File.join("config", "tickets.yaml"))

Dotenv.load
client = Discordrb::Commands::CommandBot.new(prefix: ENV["PREFIX"], token: ENV["TOKEN"])

for cog in (Dir::entries(File.join('cogs', 'drb')) - %w(. .. $))
  require_relative File.join('cogs', 'drb', cog)
  eval("client.include!(Cogs_#{cog.capitalize::delete_suffix('.rb')})")
end


cmds = {}
client.commands.each do |name, command|
	perms = command.attributes[:required_permissions].map {|x| x=x.to_s.gsub('_', ' ').split.map(&:capitalize).join(' ')}
  	roles = command.attributes[:required_roles].map { |x| x = (client.server(668000598221651975)).role(x).name if x.is_a? Integer }
	cmds[name.to_s] = {'desc' => (command.attributes[:description].split('||'))[0], 'syntax' => (command.attributes[:description].split('||'))[1], 'required_roles'=>roles, 'required_perms'=>perms}
end
dpycmds = JSON.parse(File.read(File.join('cmds', 'dcmds.json')))
File.write(File.join('cmds', 'rcmds.json'), cmds.to_json, mode: "w+")
client.command(:help, description: "A help command.||<cmdname> for help on specific command") do |event, cmdname|
	if cmdname == nil; break
	else
		cmdname.downcase!
		if cmds.member?(cmdname)
			event.send_embed do |e|
				e.title = "#{cmdname} (#{(cmds[cmdname])['syntax']})"
				e.description = "**Description:** #{(cmds[cmdname])['desc']}\n**Required roles:** #{((cmds[cmdname])['required_roles']).join(',')}\n**Required permissions:** #{((cmds[cmdname])['required_perms']).join(',')}"

				e.color = 0x0a7187
			end
		elsif dpycmds.member?(cmdname)
			event.send_embed do |e|
				e.title = "#{cmdname} (#{(dpycmds[cmdname])['syntax']})"
              	e.description = "**Description:** #{(dpycmds[cmdname])['desc']}"
				e.color = 0x0a7187
			end
		else
			event.send_embed do |e|
				e.title = "Error"
				e.description = "Command not found! Please make sure you spelled it right. See $help to see available commands."
				e.color = 0x7C0A02
			end
		end
	end
end
client.run()

