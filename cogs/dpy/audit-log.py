@client.listen()
async def on_message_delete(message):
    log_channel = get(message.guild.channels, name='📘log')
    try:
        embed = discord.Embed(title='Message Deleted 🗑️', color=discord.Color.from_rgb(178, 34, 34))
        embed.add_field(name='Content:', value=message.content)
        embed.add_field(name='Channel:', value=message.channel.mention)
        embed.set_footer(text=f'Deleted message author: {message.author}', icon_url=message.author.avatar_url)
    except:
        embed = discord.Embed(title='Message Deleted 🗑️', color=discord.Color.from_rgb(178, 34, 34),
                                description='Unable to recover')

    await log_channel.send(embed=embed)

@client.listen()
async def on_message_edit(old_message, new_message):
    if old_message.content != new_message.content:
        log_channel = get(old_message.guild.channels, name='📘log')
        if 'discord.gg/' in new_message.content.lower() or 'discordapp.com/invite' in new_message.content.lower():
            if new_message.author.id != 300126997718237195:
                await new_message.delete()
        try:
            embed = discord.Embed(title='Message Updated ♻️', color=discord.Color.from_rgb(178, 34, 34))
            embed.add_field(name='Old:', value=old_message.content)
            embed.add_field(name='New:', value=new_message.content)
            embed.add_field(name='Channel:', value=old_message.channel.mention)
            embed.set_footer(text=f'Message updated by: {old_message.author}', icon_url=old_message.author.avatar_url)
        except:
            embed = discord.Embed(title='Message Updated ♻️', color=discord.Color.from_rgb(178, 34, 34),
                                    description='Failed to display contents')

        await log_channel.send(embed=embed)

@client.listen()
async def on_message(message):
    if 'discord.gg/' in message.content.lower() or 'discordapp.com/invite' in message.content.lower():
        if message.author.id != 300126997718237195:
            await message.delete()

@client.listen()
async def on_reaction_add(reaction, user):
    log_channel = get(reaction.message.guild.channels, name='📘log')
    embed = discord.Embed(title=f'Reaction added {reaction.emoji}', color=discord.Color.from_rgb(0, 255, 0))
    embed.add_field(name='Message:', value=reaction.message.content)
    embed.add_field(name='Channel:', value=reaction.message.channel.mention)
    embed.set_footer(text=f'Reaction added by: {user}', icon_url=user.avatar_url)
    await log_channel.send(embed=embed)

@client.listen()
async def on_reaction_remove(reaction, user):
    log_channel = get(reaction.message.guild.channels, name='📘log')
    embed = discord.Embed(title=f'Reaction removed {reaction.emoji}', color=discord.Color.from_rgb(178, 34, 34))
    embed.add_field(name='Message:', value=reaction.message.content)
    embed.add_field(name='Channel:', value=reaction.message.channel.mention)
    embed.set_footer(text=f'Reaction removed by: {user}', icon_url=user.avatar_url)
    await log_channel.send(embed=embed)
