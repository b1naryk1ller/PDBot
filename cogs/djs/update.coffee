shelljs = require 'shelljs'

module.exports = 
  name: 'update'
  description: 'Updates the bot'
  required_roles: []
  required_perms: []
  execute: (msg, args) ->
    if msg.member.roles.has('682105898642047233') or msg.member.roles.has('673405620527038478')
      msg.channel.send "Restarting the bot..."
      shelljs.exec './start.sh'
    else
      message.reply("You do not have permission to access this command...")
      return